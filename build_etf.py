import pandas as pd
import requests
import json

def build_etf(symbols):
    """ symbols are id from CG"""

    df = pd.DataFrame(columns=['cg id', 'binance symbol', 'price', 'mcap', 'percent aloc'])

    coin_list_response = requests.get("https://api.coingecko.com/api/v3/coins/list")

    if coin_list_response.status_code != 200:
        raise Exception('Error retrieving cg coins list')

    coin_lists = json.loads(coin_list_response.text)


    coins = list(filter( lambda x: x['id'].lower() in symbols, coin_lists))

    if len(coins) != len(symbols):
        raise Exception('Symbols & cg coins list mismatch')

    binance_futs_tickers_response = requests.get("https://api.coingecko.com/api/v3/derivatives/exchanges/binance_futures?include_tickers='unexpired'")

    if binance_futs_tickers_response.status_code != 200:
        raise Exception('Error retrieving Binance futs tickers list')

    binance_futs_tickers = json.loads(binance_futs_tickers_response.text)['tickers']
    for coin in coins:
        ticker = list(filter(lambda x: x['base'] == coin['symbol'].upper(), binance_futs_tickers))
        if not ticker:
            raise Exception(f'{coin["name"]} not found in binance futs')
        else:
            df = df.append({'cg id': coin['id'], 'binance symbol': ticker[0]['base']}, ignore_index=True)


    symbol_strings = ','.join(df['cg id'].to_list())
    coin_mcap_list_response = requests.get(f"https://api.coingecko.com/api/v3/simple/price?ids={symbol_strings}&vs_currencies=usd&include_market_cap=true")

    if coin_mcap_list_response.status_code != 200:
        raise Exception('Error retrieving cg mcaps')

    coin_mcaps = coin_mcap_list_response.json()
    for k,v in coin_mcaps.items():
        df.loc[df['cg id'] == k, 'price'] = v['usd']
        df.loc[df['cg id'] == k, 'mcap'] = v['usd_market_cap']


    mcap_sum = df.mcap.sum()


    df['percent aloc'] = df.apply(lambda x: x['mcap'] / mcap_sum, axis=1)

    return df