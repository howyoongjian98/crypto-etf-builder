from db.context import sqlite_wrapper

@sqlite_wrapper
def get_etf(cur, etf_name):
    cur.execute("SELECT rowid FROM ETF WHERE NAME=?", (etf_name,))

    try:
        etf = dict(cur.fetchone())
    except:
        raise Exception('ETF doesn\'t exist')    

    return etf

@sqlite_wrapper
def get_positions(cur, etf_name):
    cur.execute("SELECT rowid FROM ETF WHERE NAME=?", (etf_name,))

    etf_id = get_etf(etf_name)['rowid']

    cur.execute("SELECT * from POSITION WHERE etf_id=?", (etf_id,))

    try:
        positions = cur.fetchall()
    except TypeError:
        raise Exception(f'Positions doesn\'t exist for {etf_name}')

    positions = [dict(position) for position in positions]

    return positions