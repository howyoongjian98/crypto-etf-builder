import sqlite3

class SQLiteManager():
    def __init__(self, file='db/storage/dev.db'):
        self.file=file
    def __enter__(self):
        self.conn = sqlite3.connect(self.file)
        self.conn.row_factory = sqlite3.Row
        return self.conn.cursor()
    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()


def sqlite_wrapper(func, *args, **kwargs):
    def wrapper(*args, **kwargs):
        with SQLiteManager() as cur:
            return func(cur, *args, **kwargs)

    return wrapper

