

from db.context import sqlite_wrapper


@sqlite_wrapper
def create_etf(cur, etf_name):
    cur.execute('INSERT INTO ETF (name, status) VALUES (?, ?)',
                [etf_name, 'created'])

    return cur.lastrowid


@sqlite_wrapper
def redeem_etf(cur, etf_name):
    cur.execute('UPDATE etf SET status=? WHERE name=? ',
                ['redeemed', etf_name])

    return cur.lastrowid


@sqlite_wrapper
def save_new_position(cur, etf_id, position):
    arg_list = [position['updateTime'], position['symbol'],
                position['avgPrice'], position['orderId'], position['executedQty'], etf_id]

    insert_statement = """INSERT INTO POSITION (datetime, symbol, open_price, order_id, quantity, etf_id) \
            VALUES (?, ?, ?, ?, ?, ?)"""

    cur.execute(insert_statement, arg_list)


@sqlite_wrapper
def update_positions_price(cur, positions):

    for position in positions:
        cur.execute('UPDATE POSITION SET close_price=? WHERE order_id=?', [
                    position['close_price'], position['order_id']])
