import sqlite3
con = sqlite3.connect('db/storage/dev.db')

cur = con.cursor()

cur.execute('''CREATE TABLE etf
               (
                datetime DEFAULT CURRENT_TIMESTAMP,
                name TEXT NOT NULL UNIQUE,
                status TEXT NOT NULL
               )''')

cur.execute('''CREATE TABLE position
               (
                  datetime DEFAULT CURRENT_TIMESTAMP,
                  symbol TEXT NOT NULL,
                  open_price REAL NOT NULL,
                  close_price REAL,
                  order_id INT NOT NULL,
                  etf_id INT NOT NULL,
                  quantity REAL NOT NULL,
                  foreign key(etf_id)
                        references etf (etf_id)
               )''')

con.close()
