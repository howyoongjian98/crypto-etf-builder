import time
import random

import pandas as pd
from binance import Client
from binance.enums import *
import config
from build_etf import build_etf
from db.transaction import save_new_position, create_etf as db_create_etf, redeem_etf as db_redeem_etf, update_positions_price
from db.query import get_positions

client = Client(config.API_KEY, config.API_SECRET)

def market_order(side, quantity, symbol, **kwargs):
    order_type = ORDER_TYPE_MARKET
    try:
        print(f"sending order {order_type} - {side} {quantity} {symbol}")
        order = client.futures_create_order(
            symbol=symbol, side=side, type=order_type,  quantity=quantity, **kwargs)

    except Exception as e:
        print("an exception occured - {}".format(e))
        return False

    time.sleep(1)
    order = client.futures_get_order(
        symbol=order['symbol'], orderId=order['orderId'])

    return order

def save_new_positions(etf_id, filled_orders):
    [save_new_position(etf_id, order) for order in filled_orders]

def create_etf(etf_name, symbol_list, notional_size, dry_run=False):
    etf_id = db_create_etf(etf_name)

    etf_df = build_etf(symbol_list)
    filled_orders = []
    for i in range(len(etf_df)):
        row = etf_df.iloc[i]
        position_size =  round(row['percent aloc'] * notional_size / row['price'], 3)
        symbol = f'{row["binance symbol"]}USDT'
        if dry_run:
            filled_orders.append({'symbol': symbol, 'updateTime': 1000, 'avgPrice': row['price'], 'executedQty': position_size,  'orderId': random.randint(1, 1000000)})
        else:
            filled_orders.append(market_order(SIDE_BUY, position_size, symbol))

    return save_new_positions(etf_id, filled_orders)

def redeem_etf(etf_name, dry_run=False):
    # get positions
    positions = get_positions(etf_name)
    # close positions
    for i, position in enumerate(positions):
        if dry_run:
            positions[i]['close_price'] = round(position['open_price'] * 1.1, 3)
        else:
            positions[i]['close_price'] = market_order(SIDE_SELL, position['quantity'], position['symbol'])['avgPrice']

    # update status on etf instance
    db_redeem_etf(etf_name)

    # update close_price on position
    update_positions_price(positions)
    
    # maybe calculate pnl?

# print(client.futures_get_order(symbol='ADAUSDT'))
# create_etf('ada3', ['cardano', 'ethereum'], 500, dry_run=True)
redeem_etf('ada3', dry_run=True)
# print(get_positions('adas'))